package com;

import java.awt.Image;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;

import com.data.Player;
import com.frame.GameFrame;
import com.thread.MusicWorker;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;

public class GameApplication {
	
	/**
	 * The player.
	 */
	private static Player player;
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				GameFrame.init();
			}
		});
		new JFXPanel(); //Initializes the toolkit
		Platform.runLater(new Runnable() {
			public void run() {
				MusicWorker.start();
			}
		});
	}
	
	public static Image loadImage(String path) {
		try {
			URL url = GameApplication.class.getResource(path);
			return ImageIO.read(url);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Sets the player object.
	 * @param pl The player object.
	 */
	public static void setPlayer(Player pl) {
		player = pl;
	}
	
	/**
	 * Gets the current player object.
	 * @return The current player.
	 */
	public static Player getPlayer() {
		return player;
	}
	
}