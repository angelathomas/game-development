package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import com.Constants;
import com.GameApplication;
import com.frame.GameFrame;

public class ProjectManagerChoose extends JPanel {
	private JPanel imagePanel;
	private JLabel ChloeImageLabel, EarlImageLabel, infoLabel;
	private ImageIcon ChloeImage, EarlImage;

	public ProjectManagerChoose() {
		setSize(1000, 720);
		setLayout(new BorderLayout());
		setBackground(Color.white);

		infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
		infoLabel.setBounds(100, 100, 600, 250);
		infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
		add(infoLabel, BorderLayout.NORTH);
		
		JTextArea ChloesSkills = new JTextArea();
		ChloesSkills.setFont(Constants.APP_FONT.deriveFont(16F));
		ChloesSkills.setPreferredSize(new Dimension(200, 720));
		ChloesSkills.setEditable(false);
		ChloesSkills.setLineWrap(true);
		ChloesSkills.setWrapStyleWord(true);
		ChloesSkills.setText(getChloesSkills());
		
		JTextArea EarlsSkills = new JTextArea();
		EarlsSkills.setFont(Constants.APP_FONT.deriveFont(16F));
		EarlsSkills.setPreferredSize(new Dimension(200, 720));
		EarlsSkills.setEditable(false);
		EarlsSkills.setLineWrap(true);
		EarlsSkills.setWrapStyleWord(true);
		EarlsSkills.setText(getEarlsSkills());
		
		imagePanel = new JPanel();
		imagePanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();

		Image jImage = GameApplication.loadImage("../Chloe.png");
		jImage = jImage.getScaledInstance(300, 600, Image.SCALE_SMOOTH);
		ChloeImage = new ImageIcon(jImage);
		ChloeImageLabel = new JLabel();
		ChloeImageLabel.setIcon(ChloeImage);
		
		constraints.gridx = 0;
		constraints.gridy = 1;
		imagePanel.add(ChloeImageLabel, constraints);
		
		Image bImage = GameApplication.loadImage("../Earl.png");
		bImage = bImage.getScaledInstance(300, 600, Image.SCALE_SMOOTH);
		EarlImage = new ImageIcon(bImage);
		EarlImageLabel = new JLabel();
		EarlImageLabel.setIcon(EarlImage);

		constraints.gridx = 1;
		constraints.gridy = 1;
		imagePanel.add(EarlImageLabel, constraints);
		
		JButton ChloeButton = new JButton("HIRE CHLOE");
		ChloeButton.setBackground(Color.pink);
		ChloeButton.setForeground(Color.white);
		ChloeButton.setFocusPainted(false);
		ChloeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(null, "The person you chose doesn't have enough qualifications to be your project manager!", "Invalid choice", JOptionPane.ERROR_MESSAGE);
			}
		});
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.ipady = 40;
		imagePanel.add(ChloeButton, constraints);
		
		JButton EarlButton = new JButton("HIRE EARL");
		EarlButton.setBackground(Color.pink);
		EarlButton.setForeground(Color.white);
		EarlButton.setFocusPainted(false);
		EarlButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				GameFrame.setScreen(new WriterScreen());
			}
		});
		
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.ipady = 40;
		imagePanel.add(EarlButton, constraints);
		
		add(ChloesSkills, BorderLayout.WEST);
		add(imagePanel, BorderLayout.CENTER);
		add(EarlsSkills, BorderLayout.EAST);
	}
	
	public String getChloesSkills() {
		String message = "CHLOE \n\n";
		message += "+Currently works as the head organizer of a game show\n";
		message += "\n";
		message += "+Loves to lead people\n";
		message += "\n";
		message += "+Negative attitude when stressed\n";
		message += "\n";
		message += "+Not the best at understanding people\n";
		return message;
	}
	
	public String getEarlsSkills() {
		String message = "EARL \n\n";
		message += "+Erudite of videogames\n";
		message += "\n";
		message += "+Can work on many tasks at the same time and is very persistant\n";
		message += "\n";
		message += "+Has 0 knowledge of types of energy and engineering careers \n";
		message += "\n";
		message += "+Has strong leadership skills\n";
		return message;
	}

	public String getInstructions() {
		//Concatenation or concat - which means adding value to an existing value
		//in a string variable.
		String message = "<html><b>CHOOSE YOUR PROJECT MANAGER</b><br/>";
		message += "<p>               " + "HIRE EITHER CHLOE OR EARL</p><br/>";
		message += "</html>";
		return message;
	}
}
