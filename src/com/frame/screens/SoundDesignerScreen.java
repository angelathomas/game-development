package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.Constants;
import com.frame.GameFrame;

public class SoundDesignerScreen extends JPanel {
		
		private JLabel infoLabel;
		
		private JButton nextButton;
		
		public SoundDesignerScreen() {
			setSize(1000, 720);
			setLayout(new BorderLayout());
			setBackground(Color.white);
			
			infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
			infoLabel.setBounds(100, 100, 600, 250);
			infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
			add(infoLabel, BorderLayout.CENTER);
			
			nextButton = new JButton("NEXT");
			nextButton.setBackground(Color.pink);
			nextButton.setForeground(Color.white);
			nextButton.setFocusPainted(false);
			nextButton.setFont(Constants.APP_FONT.deriveFont(20F));
			nextButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					GameFrame.setScreen(new SoundDesignerChoose());
				}
			});
			add(nextButton, BorderLayout.SOUTH);
		}
		
		public String getInstructions() {
			//Concatenation or concat - which means adding value to an existing value
			//in a string variable.
			String message = "<html><header>SOUND DESIGNERS</header><br/>";
			message += "Sounds allow players to live in an atmosphere that best fit the game they are playing.<br/>";
			message += "Sound designers will be composing sound effects and background music for the game.<br/>";
			message += "Sound designers should be able to compose music, have sound engineering skills,<br/>";
			message += "be able to work under pressure from strict deadlines,<br/>";
			message += "have good communication skills, and have a sense of timing.<br/>";

			message += "</html>";
			return message;
		}
	
}
