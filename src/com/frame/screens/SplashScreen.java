package com.frame.screens;

import java.awt.Color;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.Constants;
import com.frame.GameFrame;

public class SplashScreen extends JPanel {
	
	private JLabel titleNameLabel;
	
	private JPanel startButtonPanel;
	
	private JButton startButton;
	
	public SplashScreen() {
		setSize(1000, 720);
		setLayout(null);
		setBackground(Color.white);
		
		titleNameLabel = new JLabel("GAME TEAM CREATOR");
		titleNameLabel.setBackground(Color.white);
		titleNameLabel.setForeground(Color.pink);
		titleNameLabel.setFont(Constants.APP_FONT.deriveFont(60F));
		titleNameLabel.setBounds (170, 100, 800, 150);

		startButton = new JButton("START");
		startButton.setBackground(Color.pink);
		startButton.setForeground(Color.white);
		startButton.setFocusPainted(false);
		startButton.setFont(Constants.APP_FONT.deriveFont(20F));
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				GameFrame.setScreen(new IntroductionScreen());
			}
		});
		
		startButtonPanel = new JPanel();
		startButtonPanel.setBounds(380, 400, 200, 90);
		startButtonPanel.setBackground(Color.white);
		startButtonPanel.add(startButton);
		
		add(titleNameLabel);
		add(startButtonPanel);
	}

}