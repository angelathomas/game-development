package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.Constants;
import com.frame.GameFrame;

public class WriterScreen extends JPanel {
	
	private JLabel infoLabel;
	
	private JButton nextButton;
	
	public WriterScreen() {
		setSize(1000, 720);
		setLayout(new BorderLayout());
		setBackground(Color.white);
		
		infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
		infoLabel.setBounds(100, 100, 600, 250);
		infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
		add(infoLabel, BorderLayout.CENTER);
		
		nextButton = new JButton("NEXT");
		nextButton.setBackground(Color.pink);
		nextButton.setForeground(Color.white);
		nextButton.setFocusPainted(false);
		nextButton.setFont(Constants.APP_FONT.deriveFont(20F));
		nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				GameFrame.setScreen(new WriterChoose());
			}
		});
		add(nextButton, BorderLayout.SOUTH);
	}
	
	public String getInstructions() {
		//Concatenation or concat - which means adding value to an existing value
		//in a string variable.
		String message = "<html><header>WRITERS</header><br/>";
		message += "Games are much better written by professional writers that have spent years<br/>";
		message += "perfecting their skills verses giving the writing duty to the programmer, artist,<br/>";
		message += "or any one else on the team. Writers create narrative elements such<br/>";
		message += "as the plot, characters, settings, etc. The writer also makes sure that the game <br/>";
		message += "has a captivating storyline and dialouge that fits the videogame.<br/>";
		message += "Writers need to show creativity in their writing, have good communication and presentation<br/>";
		message += "skills, and create dialouge that suits the game's character and atmosphere.<br/>";
		
		message += "</html>";
		return message;
	}
}
