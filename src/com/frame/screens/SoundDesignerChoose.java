package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import com.Constants;
import com.GameApplication;
import com.frame.GameFrame;

public class SoundDesignerChoose extends JPanel {
	private JPanel imagePanel;
	private JLabel bobbyImageLabel, goblinImageLabel, infoLabel;
	private ImageIcon bobbyImage, goblinImage;

	public SoundDesignerChoose() {
		setSize(1000, 720);
		setLayout(new BorderLayout());
		setBackground(Color.white);

		infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
		infoLabel.setBounds(100, 100, 600, 250);
		infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
		add(infoLabel, BorderLayout.NORTH);
		
		JTextArea GoblinsSkills = new JTextArea();
		GoblinsSkills.setFont(Constants.APP_FONT.deriveFont(16F));
		GoblinsSkills.setPreferredSize(new Dimension(200, 720));
		GoblinsSkills.setEditable(false);
		GoblinsSkills.setLineWrap(true);
		GoblinsSkills.setWrapStyleWord(true);
		GoblinsSkills.setText(getGoblinsSkills());
		
		JTextArea BobbysSkills = new JTextArea();
		BobbysSkills.setFont(Constants.APP_FONT.deriveFont(16F));
		BobbysSkills.setPreferredSize(new Dimension(200, 720));
		BobbysSkills.setEditable(false);
		BobbysSkills.setLineWrap(true);
		BobbysSkills.setWrapStyleWord(true);
		BobbysSkills.setText(getBobbysSkills());
		
		imagePanel = new JPanel();
		imagePanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();

		Image jImage = GameApplication.loadImage("../Goblin.png");
		jImage = jImage.getScaledInstance(300, 600, Image.SCALE_SMOOTH);
		goblinImage = new ImageIcon(jImage);
		goblinImageLabel = new JLabel();
		goblinImageLabel.setIcon(goblinImage);
		
		constraints.gridx = 0;
		constraints.gridy = 1;
		imagePanel.add(goblinImageLabel, constraints);
		
		Image bImage = GameApplication.loadImage("../Bobby.png");
		bImage = bImage.getScaledInstance(300, 600, Image.SCALE_SMOOTH);
		bobbyImage = new ImageIcon(bImage);
		bobbyImageLabel = new JLabel();
		bobbyImageLabel.setIcon(bobbyImage);

		constraints.gridx = 1;
		constraints.gridy = 1;
		imagePanel.add(bobbyImageLabel, constraints);
		
		JButton GoblinButton = new JButton("HIRE GOBLIN");
		GoblinButton.setBackground(Color.pink);
		GoblinButton.setForeground(Color.white);
		GoblinButton.setFocusPainted(false);
		GoblinButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				GameFrame.setScreen(new TesterScreen());
			}
		});
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.ipady = 40;
		imagePanel.add(GoblinButton, constraints);
		
		JButton bobbyButton = new JButton("HIRE BOB");
		bobbyButton.setBackground(Color.pink);
		bobbyButton.setForeground(Color.white);
		bobbyButton.setFocusPainted(false);
		bobbyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(null, "The person you chose doesn't have enough qualifications to be your sound designer!", "Invalid choice", JOptionPane.ERROR_MESSAGE);
			}
		});
		
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.ipady = 40;
		imagePanel.add(bobbyButton, constraints);
		
		add(GoblinsSkills, BorderLayout.WEST);
		add(imagePanel, BorderLayout.CENTER);
		add(BobbysSkills, BorderLayout.EAST);
	}
	
	public String getGoblinsSkills() {
		String message = "GOBLIN \n\n";
		message += "+Works well under pressure\n";
		message += "\n";
		message += "+Composes his own music\n";
		message += "\n";
		message += "+Horrible at literature\n";
		message += "\n";
		message += "+Knows about modern sound technology\n";
		return message;
	}
	
	public String getBobbysSkills() {
		String message = "BOB \n\n";
		message += "+Plays the kazoo\n";
		message += "\n";
		message += "+Listens to music on the bus almost everyday\n";
		message += "\n";
		message += "+Doesn't know much about modern sound technology\n";
		message += "\n";
		message += "+Currently works as a studio manager\n";
		message += "\n";
		message += "+Creative\n";
		return message;
	}

	public String getInstructions() {
		//Concatenation or concat - which means adding value to an existing value
		//in a string variable.
		String message = "<html><b>CHOOSE YOUR SOUND DESIGNER</b><br/>";
		message += "<p>               " + "HIRE EITHER THE GOBLIN OR BOB</p><br/>";
		message += "</html>";
		return message;
	}
}
