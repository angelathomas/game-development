package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import com.Constants;
import com.GameApplication;
import com.frame.GameFrame;

public class WriterChoose extends JPanel{
	private JPanel imagePanel;
	private JLabel GeorgeImageLabel, LilyImageLabel, infoLabel;
	private ImageIcon GeorgeImage, LilyImage;

	public WriterChoose() {
		setSize(1000, 720);
		setLayout(new BorderLayout());
		setBackground(Color.white);

		infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
		infoLabel.setBounds(100, 100, 600, 250);
		infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
		add(infoLabel, BorderLayout.NORTH);
		
		JTextArea GeorgesSkills = new JTextArea();
		GeorgesSkills.setFont(Constants.APP_FONT.deriveFont(16F));
		GeorgesSkills.setPreferredSize(new Dimension(200, 720));
		GeorgesSkills.setEditable(false);
		GeorgesSkills.setLineWrap(true);
		GeorgesSkills.setWrapStyleWord(true);
		GeorgesSkills.setText(getGeorgesSkills());
		
		JTextArea LilysSkills = new JTextArea();
		LilysSkills.setFont(Constants.APP_FONT.deriveFont(16F));
		LilysSkills.setPreferredSize(new Dimension(200, 720));
		LilysSkills.setEditable(false);
		LilysSkills.setLineWrap(true);
		LilysSkills.setWrapStyleWord(true);
		LilysSkills.setText(getLilysSkills());
		
		imagePanel = new JPanel();
		imagePanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();

		Image jImage = GameApplication.loadImage("../George.png");
		jImage = jImage.getScaledInstance(300, 600, Image.SCALE_SMOOTH);
		GeorgeImage = new ImageIcon(jImage);
		GeorgeImageLabel = new JLabel();
		GeorgeImageLabel.setIcon(GeorgeImage);
		
		constraints.gridx = 0;
		constraints.gridy = 1;
		imagePanel.add(GeorgeImageLabel, constraints);
		
		Image bImage = GameApplication.loadImage("../Lily.png");
		bImage = bImage.getScaledInstance(300, 600, Image.SCALE_SMOOTH);
		LilyImage = new ImageIcon(bImage);
		LilyImageLabel = new JLabel();
		LilyImageLabel.setIcon(LilyImage);

		constraints.gridx = 1;
		constraints.gridy = 1;
		imagePanel.add(LilyImageLabel, constraints);
		
		JButton GeorgeButton = new JButton("HIRE GEORGE");
		GeorgeButton.setBackground(Color.pink);
		GeorgeButton.setForeground(Color.white);
		GeorgeButton.setFocusPainted(false);
		GeorgeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				GameFrame.setScreen(new OutroScreen());
			}
		});
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.ipady = 40;
		imagePanel.add(GeorgeButton, constraints);
		
		JButton LilyButton = new JButton("HIRE LILY");
		LilyButton.setBackground(Color.pink);
		LilyButton.setForeground(Color.white);
		LilyButton.setFocusPainted(false);
		LilyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(null, "The person you chose doesn't have enough qualifications to be your writer!", "Invalid choice", JOptionPane.ERROR_MESSAGE);
			}
		});
		
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.ipady = 40;
		imagePanel.add(LilyButton, constraints);
		
		add(GeorgesSkills, BorderLayout.WEST);
		add(imagePanel, BorderLayout.CENTER);
		add(LilysSkills, BorderLayout.EAST);
	}
	
	public String getGeorgesSkills() {
		String message = "GEORGE \n\n";
		message += "+Currently works as an author\n";
		message += "\n";
		message += "+Has years of experience in newspaper editing\n";
		message += "\n";
		message += "+Horrible at math and science related subjects\n";
		message += "\n";
		message += "+Creative and has good presentation skills\n";
		return message;
	}
	
	public String getLilysSkills() {
		String message = "LILY \n\n";
		message += "+Working as a lawyer\n";
		message += "\n";
		message += "+Loves literature and Shakespeare most of all\n";
		message += "\n";
		message += "+Has strong leadership and communication skills\n";
		return message;
	}

	public String getInstructions() {
		//Concatenation or concat - which means adding value to an existing value
		//in a string variable.
		String message = "<html><b>CHOOSE YOUR WRITER</b><br/>";
		message += "<p>               " + "HIRE EITHER GEORGE OR LILY</p><br/>";
		message += "</html>";
		return message;
	}

}
