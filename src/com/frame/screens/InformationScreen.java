package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.Constants;
import com.GameApplication;
import com.frame.GameFrame;

public class InformationScreen extends JPanel {
	
	private JLabel instructionLabel;
	
	private JButton nextButton;
	
	public InformationScreen() {
		setSize(1000, 720);
		setLayout(new BorderLayout());
		setBackground(Color.white);
		
		instructionLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
		instructionLabel.setBounds(100, 100, 600, 250);
		instructionLabel.setFont(Constants.APP_FONT.deriveFont(20F));
		add(instructionLabel, BorderLayout.CENTER);
		
		nextButton = new JButton("NEXT");
		nextButton.setBackground(Color.pink);
		nextButton.setForeground(Color.white);
		nextButton.setFocusPainted(false);
		nextButton.setFont(Constants.APP_FONT.deriveFont(20F));
		nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				GameFrame.setScreen(new DeveloperScreen());
			}
		});
		add(nextButton, BorderLayout.SOUTH);
	}
	
	public String getInstructions() {
		//Concatenation or concat - which means adding value to an existing value
		//in a string variable.
		String message = "<html>Hello " + GameApplication.getPlayer().getName() + "!<br/>";
		message += "You are a starting business producer who is looking <br/>";
		message += "to invest your money in a small indie game development team.<br/>";
		message += "Sadly, you have no clue about who you'll need to hire to start your team.<br/>";
		message += "Luckily, this game will be telling you a thing or two<br/>";
		message += "about who to hire on your team so that you can start getting profit.<br/>";
		message += "The game will be explaining what skills each person will need to successfully<br/>";
		message += "implement your indie game. Then, you'll be picking who is the most<br/>";
		message += "suitable person for the position.<br/>";
		message += "</html>";
		return message;
	}

}