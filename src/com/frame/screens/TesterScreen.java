package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.Constants;
import com.frame.GameFrame;

public class TesterScreen extends JPanel {
	
	private JLabel infoLabel;
	
	private JButton nextButton;
	
	public TesterScreen() {
		setSize(1000, 720);
		setLayout(new BorderLayout());
		setBackground(Color.white);
		
		infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
		infoLabel.setBounds(100, 100, 600, 250);
		infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
		add(infoLabel, BorderLayout.CENTER);
		
		nextButton = new JButton("NEXT");
		nextButton.setBackground(Color.pink);
		nextButton.setForeground(Color.white);
		nextButton.setFocusPainted(false);
		nextButton.setFont(Constants.APP_FONT.deriveFont(20F));
		nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				GameFrame.setScreen(new TesterChoose());
			}
		});
		add(nextButton, BorderLayout.SOUTH);
	}
	public String getInstructions() {
		//Concatenation or concat - which means adding value to an existing value
		//in a string variable.
		String message = "<html><header>GAME QUALITY ASSURANCE TESTERS</header><br/>";
		message += "Games more often than not end up having bugs or strange glitches that can infuriate <br/>";
		message += "players enough to give bad reviews on the game.</br>";
		message += "To prevent this from happening, game testers <br/>";
		message += "play the game to ensure that<br/>";
		message += "there are no bugs or wierd glitches present. <br/>";
		message += "Game testers must pay close attention to details, have a passion for <br/>";
		message += "playing video games, be able to play games for long time frames, be analytical, <br/>";
		message += "have good problem-solving skills, and can evaluate a game against its competition. <br/>";
		message += "</html>";
		return message;
	}
}
