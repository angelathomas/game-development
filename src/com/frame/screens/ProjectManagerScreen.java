package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.Constants;
import com.frame.GameFrame;

public class ProjectManagerScreen extends JPanel {
	
	private JLabel infoLabel;
	
	private JButton nextButton;
	
	public ProjectManagerScreen() {
		setSize(1000, 720);
		setLayout(new BorderLayout());
		setBackground(Color.white);
		
		infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
		infoLabel.setBounds(100, 100, 600, 250);
		infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
		add(infoLabel, BorderLayout.CENTER);
		
		nextButton = new JButton("NEXT");
		nextButton.setBackground(Color.pink);
		nextButton.setForeground(Color.white);
		nextButton.setFocusPainted(false);
		nextButton.setFont(Constants.APP_FONT.deriveFont(20F));
		nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				GameFrame.setScreen(new ProjectManagerChoose());
			}
		});
		add(nextButton, BorderLayout.SOUTH);
	}
	
	public String getInstructions() {
		//Concatenation or concat - which means adding value to an existing value
		//in a string variable.
		String message = "<html><header>PROJECT MANAGERS</header><br/>";
		message += "Making something as hard as videogames is stressful and make people easily<br/>";
		message += "less motivated to continue. Project managers makes sure that the team<br/>";
		message += "stays focused and work efficiently. Project managers set reasonable<br/>";
		message += "deadlines for the team and while doing so inspire the team to keep moving <br/>";
		message += "forward. Project managers should have strong leadership and communication<br/>";
		message += "skills, be able to multitask, and have a vast knowledge of videogames.<br/>";
		
		message += "</html>";
		return message;
	}
	
}