package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import com.Constants;
import com.GameApplication;
import com.frame.GameFrame;

public class ArtistChoose extends JPanel{
	private JPanel imagePanel;
	private JLabel jenImageLabel, draculaImageLabel, infoLabel;
	private ImageIcon jenImage, draculaImage;

	public ArtistChoose() {
		setSize(1000, 720);
		setLayout(new BorderLayout());
		setBackground(Color.white);

		infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
		infoLabel.setBounds(100, 100, 600, 250);
		infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
		add(infoLabel, BorderLayout.NORTH);
		
		JTextArea jensSkills = new JTextArea();
		jensSkills.setFont(Constants.APP_FONT.deriveFont(16F));
		jensSkills.setPreferredSize(new Dimension(200, 720));
		jensSkills.setEditable(false);
		jensSkills.setLineWrap(true);
		jensSkills.setWrapStyleWord(true);
		jensSkills.setText(getJensSkills());
		
		JTextArea draculasSkills = new JTextArea();
		draculasSkills.setFont(Constants.APP_FONT.deriveFont(16F));
		draculasSkills.setPreferredSize(new Dimension(200, 720));
		draculasSkills.setEditable(false);
		draculasSkills.setLineWrap(true);
		draculasSkills.setWrapStyleWord(true);
		draculasSkills.setText(getDraculasSkills());
		
		imagePanel = new JPanel();
		imagePanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();

		Image jImage = GameApplication.loadImage("../Jen.png");
		jImage = jImage.getScaledInstance(300, 600, Image.SCALE_SMOOTH);
		jenImage = new ImageIcon(jImage);
		jenImageLabel = new JLabel();
		jenImageLabel.setIcon(jenImage);
		
		constraints.gridx = 0;
		constraints.gridy = 1;
		imagePanel.add(jenImageLabel, constraints);
		
		Image bImage = GameApplication.loadImage("../Dracula.png");
		bImage = bImage.getScaledInstance(300, 600, Image.SCALE_SMOOTH);
		draculaImage = new ImageIcon(bImage);
		draculaImageLabel = new JLabel();
		draculaImageLabel.setIcon(draculaImage);

		constraints.gridx = 1;
		constraints.gridy = 1;
		imagePanel.add(draculaImageLabel, constraints);
		
		JButton jenButton = new JButton("HIRE JEN");
		jenButton.setBackground(Color.pink);
		jenButton.setForeground(Color.white);
		jenButton.setFocusPainted(false);
		jenButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				GameFrame.setScreen(new SoundDesignerScreen());
			}
		});
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.ipady = 40;
		imagePanel.add(jenButton, constraints);
		
		JButton draculaButton = new JButton("HIRE DRACULA");
		draculaButton.setBackground(Color.pink);
		draculaButton.setForeground(Color.white);
		draculaButton.setFocusPainted(false);
		draculaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(null, "Really :I", "Invalid choice", JOptionPane.ERROR_MESSAGE);
			}
		});
		
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.ipady = 40;
		imagePanel.add(draculaButton, constraints);
		
		add(jensSkills, BorderLayout.WEST);
		add(imagePanel, BorderLayout.CENTER);
		add(draculasSkills, BorderLayout.EAST);
	}
	
	public String getJensSkills() {
		String message = "JEN \n\n";
		message += "+Currently works as an artist \n";
		message += "\n";
		message += "+Has knowledge of many artistic techniques\n";
		message += "\n";
		message += "+Loves expirimenting with colors\n";
		message += "\n";
		message += "+Pays close attention to detail\n";
		return message;
	}
	
	public String getDraculasSkills() {
		String message = "DRACULA \n\n";
		message += "+Hates garlic\n";
		message += "\n";
		message += "+Doesn't like to be stabbed, especially in the heart\n";
		message += "\n";
		message += "+Possibly a vampire\n";
		return message;
	}

	public String getInstructions() {
		//Concatenation or concat - which means adding value to an existing value
		//in a string variable.
		String message = "<html><b>CHOOSE YOUR ARTIST</b><br/>";
		message += "<p>               " + "HIRE EITHER JEN OR DRACULA</p><br/>";
		message += "</html>";
		return message;
	}

}
