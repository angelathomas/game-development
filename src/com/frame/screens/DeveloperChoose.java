package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import com.Constants;
import com.GameApplication;
import com.frame.GameFrame;

public class DeveloperChoose extends JPanel {

	private JPanel imagePanel;
	private JLabel jeffImageLabel, bobImageLabel, infoLabel;
	private ImageIcon jeffImage, bobImage;

	public DeveloperChoose() {
		setSize(1000, 720);
		setLayout(new BorderLayout());
		setBackground(Color.white);

		infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
		infoLabel.setBounds(100, 100, 600, 250);
		infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
		add(infoLabel, BorderLayout.NORTH);
		
		JTextArea jeffSkills = new JTextArea();
		jeffSkills.setFont(Constants.APP_FONT.deriveFont(16F));
		jeffSkills.setPreferredSize(new Dimension(200, 720));
		jeffSkills.setEditable(false);
		jeffSkills.setLineWrap(true);
		jeffSkills.setWrapStyleWord(true);
		jeffSkills.setText(getJeffsSkills());
		
		JTextArea bobsSkills = new JTextArea();
		bobsSkills.setFont(Constants.APP_FONT.deriveFont(16F));
		bobsSkills.setPreferredSize(new Dimension(200, 720));
		bobsSkills.setEditable(false);
		bobsSkills.setLineWrap(true);
		bobsSkills.setWrapStyleWord(true);
		bobsSkills.setText(getBobsSkills());
		
		imagePanel = new JPanel();
		imagePanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();

		Image jImage = GameApplication.loadImage("../Jeff.png");
		jImage = jImage.getScaledInstance(300, 600, Image.SCALE_SMOOTH);
		jeffImage = new ImageIcon(jImage);
		jeffImageLabel = new JLabel();
		jeffImageLabel.setIcon(jeffImage);
		
		constraints.gridx = 0;
		constraints.gridy = 1;
		imagePanel.add(jeffImageLabel, constraints);
		
		Image bImage = GameApplication.loadImage("../Bob.png");
		bImage = bImage.getScaledInstance(300, 600, Image.SCALE_SMOOTH);
		bobImage = new ImageIcon(bImage);
		bobImageLabel = new JLabel();
		bobImageLabel.setIcon(bobImage);

		constraints.gridx = 1;
		constraints.gridy = 1;
		imagePanel.add(bobImageLabel, constraints);
		
		JButton jeffButton = new JButton("HIRE JEFF");
		jeffButton.setBackground(Color.pink);
		jeffButton.setForeground(Color.white);
		jeffButton.setFocusPainted(false);
		jeffButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				GameFrame.setScreen(new DesignerScreen());
			}
		});
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.ipady = 40;
		imagePanel.add(jeffButton, constraints);
		
		JButton bobButton = new JButton("HIRE SUBIN");
		bobButton.setBackground(Color.pink);
		bobButton.setForeground(Color.white);
		bobButton.setFocusPainted(false);
		bobButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(null, "The person you chose doesn't have enough qualifications to be your game developer!", "Invalid choice", JOptionPane.ERROR_MESSAGE);
			}
		});
		
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.ipady = 40;
		imagePanel.add(bobButton, constraints);
		
		add(jeffSkills, BorderLayout.WEST);
		add(imagePanel, BorderLayout.CENTER);
		add(bobsSkills, BorderLayout.EAST);
	}
	
	public String getJeffsSkills() {
		String message = "JEFF \n\n";
		message += "+Currently works as a programmer for big gaming companies\n";
		message += "\n";
		message += "+Horrible at learning how to make music\n";
		message += "\n";
		message += "+Has a 3.7 GPA in college\n";
		message += "\n";
		message += "+Patient and is great at math\n";
		return message;
	}
	
	public String getBobsSkills() {
		String message = "SUBIN \n\n";
		message += "+Just graduated from college\n";
		message += "\n";
		message += "+Works as an intern software developer\n";
		message += "\n";
		message += "+Has a 3.9 GPA in College\n";
		message += "\n";
		message += "+Can't get work done on time\n";
		return message;
	}

	public String getInstructions() {
		//Concatenation or concat - which means adding value to an existing value
		//in a string variable.
		String message = "<html><b>CHOOSE YOUR DEVELOPER</b><br/>";
		message += "<p>               " + "HIRE EITHER JEFF OR SUBIN</p><br/>";
		message += "</html>";
		return message;
	}

}