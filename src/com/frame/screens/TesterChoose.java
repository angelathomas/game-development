package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import com.Constants;
import com.GameApplication;
import com.frame.GameFrame;

public class TesterChoose extends JPanel {
	private JPanel imagePanel;
	private JLabel DavidImageLabel, MattImageLabel, infoLabel;
	private ImageIcon DavidImage, MattImage;

	public TesterChoose() {
		setSize(1000, 720);
		setLayout(new BorderLayout());
		setBackground(Color.white);

		infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
		infoLabel.setBounds(100, 100, 600, 250);
		infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
		add(infoLabel, BorderLayout.NORTH);
		
		JTextArea DavidsSkills = new JTextArea();
		DavidsSkills.setFont(Constants.APP_FONT.deriveFont(16F));
		DavidsSkills.setPreferredSize(new Dimension(200, 720));
		DavidsSkills.setEditable(false);
		DavidsSkills.setLineWrap(true);
		DavidsSkills.setWrapStyleWord(true);
		DavidsSkills.setText(getDavidsSkills());
		
		JTextArea MattsSkills = new JTextArea();
		MattsSkills.setFont(Constants.APP_FONT.deriveFont(16F));
		MattsSkills.setPreferredSize(new Dimension(200, 720));
		MattsSkills.setEditable(false);
		MattsSkills.setLineWrap(true);
		MattsSkills.setWrapStyleWord(true);
		MattsSkills.setText(getMattsSkills());
		
		imagePanel = new JPanel();
		imagePanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();

		Image jImage = GameApplication.loadImage("../David.png");
		jImage = jImage.getScaledInstance(300, 600, Image.SCALE_SMOOTH);
		DavidImage = new ImageIcon(jImage);
		DavidImageLabel = new JLabel();
		DavidImageLabel.setIcon(DavidImage);
		
		constraints.gridx = 0;
		constraints.gridy = 1;
		imagePanel.add(DavidImageLabel, constraints);
		
		Image bImage = GameApplication.loadImage("../Matt.png");
		bImage = bImage.getScaledInstance(300, 600, Image.SCALE_SMOOTH);
		MattImage = new ImageIcon(bImage);
		MattImageLabel = new JLabel();
		MattImageLabel.setIcon(MattImage);

		constraints.gridx = 1;
		constraints.gridy = 1;
		imagePanel.add(MattImageLabel, constraints);
		
		JButton DavidButton = new JButton("HIRE DAVID");
		DavidButton.setBackground(Color.pink);
		DavidButton.setForeground(Color.white);
		DavidButton.setFocusPainted(false);
		DavidButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(null, "The person you chose doesn't have enough qualifications to be your quality insurance tester!", "Invalid choice", JOptionPane.ERROR_MESSAGE);

			}
		});
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.ipady = 40;
		imagePanel.add(DavidButton, constraints);
		
		JButton MattButton = new JButton("HIRE MATT");
		MattButton.setBackground(Color.pink);
		MattButton.setForeground(Color.white);
		MattButton.setFocusPainted(false);
		MattButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				GameFrame.setScreen(new ProjectManagerScreen());

			}
		});
		
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.ipady = 40;
		imagePanel.add(MattButton, constraints);
		
		add(MattsSkills, BorderLayout.EAST);
		add(imagePanel, BorderLayout.CENTER);
		add(DavidsSkills, BorderLayout.WEST);
	}
	
	public String getDavidsSkills() {
		String message = "DAVID \n\n";
		message += "+Has 3 hours of gaming expierence on agar.io\n";
		message += "\n";
		message += "+Pays close attention to detail\n";
		message += "\n";
		message += "+Hates puzzles with a passion\n";
		message += "\n";
		message += "+Has good communication skills\n";
		return message;
	}
	
	public String getMattsSkills() {
		String message = "MATT \n\n";
		message += "+Has great hand-eye coordination\n";
		message += "\n";
		message += "+Has over 9000 hours of gaming expierence\n";
		message += "\n";
		message += "+Is good at math\n";
		message += "\n";
		message += "+Always compare games with each other logically \n";
		return message;
	}

	public String getInstructions() {
		//Concatenation or concat - which means adding value to an existing value
		//in a string variable.
		String message = "<html><b>CHOOSE YOUR QUALITY ASSURANCE TESTER</b><br/>";
		message += "<p>               " + "HIRE EITHER DAVID OR MATT</p><br/>";
		message += "</html>";
		return message;
	}

}
