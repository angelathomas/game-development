package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.Constants;
import com.frame.GameFrame;

public class DesignerScreen extends JPanel {
	
	private JLabel infoLabel;
	
	private JButton nextButton;
	
	public DesignerScreen() {
		setSize(1000, 720);
		setLayout(new BorderLayout());
		setBackground(Color.white);
		
		infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
		infoLabel.setBounds(100, 100, 600, 250);
		infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
		add(infoLabel, BorderLayout.CENTER);
		
		nextButton = new JButton("NEXT");
		nextButton.setBackground(Color.pink);
		nextButton.setForeground(Color.white);
		nextButton.setFocusPainted(false);
		nextButton.setFont(Constants.APP_FONT.deriveFont(20F));
		nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				GameFrame.setScreen(new DesignerChoose());
			}
		});
		add(nextButton, BorderLayout.SOUTH);
	}
	
	public String getInstructions() {
		//Concatenation or concat - which means adding value to an existing value
		//in a string variable.
		String message = "<html><header>GAME DESIGNERS</header><br/>";
		message += "Game designers are not just the idea person,<br/>";
		message += "they give the general direction of all core aspects of a game<br/>";
		message += "and communicate the objectives to the team. Game designers <br/>";
		message += "keep documents of how the game's settings,<br/>";
		message += "story flow, characters, etc. are to be presented to the players.<br/>";
		message += "They need to have good verbal and written communication skills,<br/>";
		message += "possess creativity, have some programming and artistic knowledge, and<br/>";
	    message += "have a vast knowledge of the game play theory.<br/>";		
		message += "</html>";
		return message;
	}
	
}