package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import com.Constants;
import com.GameApplication;
import com.frame.GameFrame;

public class DesignerChoose extends JPanel{
	private JPanel imagePanel;
	private JLabel PoppyImageLabel, RubyImageLabel, infoLabel;
	private ImageIcon PoppyImage, RubyImage;

	public DesignerChoose() {
		setSize(1000, 720);
		setLayout(new BorderLayout());
		setBackground(Color.white);

		infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
		infoLabel.setBounds(100, 100, 600, 250);
		infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
		add(infoLabel, BorderLayout.NORTH);
		
		JTextArea PoppysSkills = new JTextArea();
		PoppysSkills.setFont(Constants.APP_FONT.deriveFont(16F));
		PoppysSkills.setPreferredSize(new Dimension(200, 720));
		PoppysSkills.setEditable(false);
		PoppysSkills.setLineWrap(true);
		PoppysSkills.setWrapStyleWord(true);
		PoppysSkills.setText(getPoppysSkills());
		
		JTextArea RubysSkills = new JTextArea();
		RubysSkills.setFont(Constants.APP_FONT.deriveFont(16F));
		RubysSkills.setPreferredSize(new Dimension(200, 720));
		RubysSkills.setEditable(false);
		RubysSkills.setLineWrap(true);
		RubysSkills.setWrapStyleWord(true);
		RubysSkills.setText(getRubysSkills());
		
		imagePanel = new JPanel();
		imagePanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();

		Image jImage = GameApplication.loadImage("../Poppy.png");
		jImage = jImage.getScaledInstance(300, 600, Image.SCALE_SMOOTH);
		PoppyImage = new ImageIcon(jImage);
		PoppyImageLabel = new JLabel();
		PoppyImageLabel.setIcon(PoppyImage);
		
		constraints.gridx = 0;
		constraints.gridy = 1;
		imagePanel.add(PoppyImageLabel, constraints);
		
		Image bImage = GameApplication.loadImage("../Ruby.png");
		bImage = bImage.getScaledInstance(300, 600, Image.SCALE_SMOOTH);
		RubyImage = new ImageIcon(bImage);
		RubyImageLabel = new JLabel();
		RubyImageLabel.setIcon(RubyImage);

		constraints.gridx = 1;
		constraints.gridy = 1;
		imagePanel.add(RubyImageLabel, constraints);
		
		JButton PoppyButton = new JButton("HIRE POPPY");
		PoppyButton.setBackground(Color.pink);
		PoppyButton.setForeground(Color.white);
		PoppyButton.setFocusPainted(false);
		PoppyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(null, "The person you chose doesn't have enough qualifications to be your game designer!", "Invalid choice", JOptionPane.ERROR_MESSAGE);
			}
		});
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.ipady = 40;
		imagePanel.add(PoppyButton, constraints);
		
		JButton RubyButton = new JButton("HIRE RUBY");
		RubyButton.setBackground(Color.pink);
		RubyButton.setForeground(Color.white);
		RubyButton.setFocusPainted(false);
		RubyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				GameFrame.setScreen(new ArtistScreen());
			}
		});
		
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.ipady = 40;
		imagePanel.add(RubyButton, constraints);
		
		add(PoppysSkills, BorderLayout.WEST);
		add(imagePanel, BorderLayout.CENTER);
		add(RubysSkills, BorderLayout.EAST);
	}
	
	public String getPoppysSkills() {
		String message = "POPPY \n\n";
		message += "+Currently works as a artist\n";
		message += "\n";
		message += "+Has good origanizational skills\n";
		message += "\n";
		message += "+Has artistic knowledge\n";
		message += "\n";
		message += "+Loves literature and art \n";
		return message;
	}
	
	public String getRubysSkills() {
		String message = "RUBY \n\n";
		message += "+Is a game play theorist\n";
		message += "\n";
		message += "+Knows some programming languages\n";
		message += "\n";
		message += "+Knows about art techniques\n";
		message += "\n";
		message += "+Has good organizational skills\n";
		message += "\n";
		message += "+Loves Literature\n";
		return message;
	}

	public String getInstructions() {
		//Concatenation or concat - which means adding value to an existing value
		//in a string variable.
		String message = "<html><b>CHOOSE YOUR DESIGNER</b><br/>";
		message += "<p>               " + "HIRE EITHER POPPY OR RUBY</p><br/>";
		message += "</html>";
		return message;
	}

}
