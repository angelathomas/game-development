package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.Constants;
import com.GameApplication;
import com.data.Player;
import com.frame.GameFrame;

public class IntroductionScreen extends JPanel {
	
	private JLabel textLabel;
	
	private JTextField nameField;
	
	private JButton nextButton;
	
	public IntroductionScreen() {
		setSize(1000, 720);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBackground(Color.white);
		
		textLabel = new JLabel("ENTER YOUR NAME");
		textLabel.setForeground(Color.pink);
		textLabel.setAlignmentX(CENTER_ALIGNMENT);
		textLabel.setAlignmentY(CENTER_ALIGNMENT);
		textLabel.setFont(Constants.APP_FONT.deriveFont(40F));
		
		nameField = new JTextField();
		nameField.setAlignmentX(CENTER_ALIGNMENT);
		nameField.setPreferredSize(new Dimension(300, 50));
		
		JPanel nameFieldPanel = new JPanel();
		nameFieldPanel.add(nameField, BorderLayout.CENTER);
		
		nextButton = new JButton("NEXT");
		nextButton.setBackground(Color.pink);
		nextButton.setForeground(Color.white);
		nextButton.setFocusPainted(false);
		nextButton.setAlignmentX(CENTER_ALIGNMENT);
		nextButton.setFont(Constants.APP_FONT.deriveFont(20F));
		nextButton.setSize(100, 50);
		nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				String name = nameField.getText();
				if (name == null || name.isEmpty()) {
					JOptionPane.showMessageDialog(null, "Please enter the player's name to begin!");
					return;
				}
				Player player = new Player();
				player.setName(name);
				GameApplication.setPlayer(player);
				GameFrame.setScreen(new InformationScreen());
			}
		});
		
		add(textLabel);
		add(nameFieldPanel);
		add(nextButton);
	}
	
}