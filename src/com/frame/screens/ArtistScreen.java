package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.Constants;
import com.frame.GameFrame;

public class ArtistScreen extends JPanel {
		
		private JLabel infoLabel;
		
		private JButton nextButton;
		
		public ArtistScreen() {
			setSize(1000, 720);
			setLayout(new BorderLayout());
			setBackground(Color.white);
			
			infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
			infoLabel.setBounds(100, 100, 600, 250);
			infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
			add(infoLabel, BorderLayout.CENTER);
			
			nextButton = new JButton("NEXT");
			nextButton.setBackground(Color.pink);
			nextButton.setForeground(Color.white);
			nextButton.setFocusPainted(false);
			nextButton.setFont(Constants.APP_FONT.deriveFont(20F));
			nextButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					GameFrame.setScreen(new ArtistChoose());
				}
			});
			add(nextButton, BorderLayout.SOUTH);
		}
		
		public String getInstructions() {
			//Concatenation or concat - which means adding value to an existing value
			//in a string variable.
			String message = "<html><header>GAME ARTISTS</header><br/>";
			message += "If your game needs visuals, which most do, you're going to need an artist.<br/>";
			message += "An artist will be the one crafting and designing how characters, objects,<br/>";
			message += "backgrounds, animations, etc. will look like. Artists will work closely<br/>";
			message += "with programmers to make sure both reach the technical and artistic<br/>";
			message += "requirements for the game. Artists must pay close attention to details,<br/>";
			message += "possess creativity, be familiar with imaging software, and have<br/>";
			message += "knowledge of color, perspective, anatomy, architecture, etc.<br/>";
			message += "</html>";
			return message;
		}

}
