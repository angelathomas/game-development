package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.Constants;
import com.GameApplication;

public class OutroScreen extends JPanel {
	
	private JLabel infoLabel;
	
	private JButton endButton;
	
	public OutroScreen() {
		setSize(1000, 720);
		setLayout(new BorderLayout());
		setBackground(Color.white);
		
		infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
		infoLabel.setBounds(100, 100, 600, 250);
		infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
		add(infoLabel, BorderLayout.CENTER);
		
		endButton = new JButton("END");
		endButton.setBackground(Color.pink);
		endButton.setForeground(Color.white);
		endButton.setFocusPainted(false);
		endButton.setFont(Constants.APP_FONT.deriveFont(20F));
		endButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				System.exit(-1);
			}
		});

		add(endButton, BorderLayout.SOUTH);
	}
	public String getInstructions() {
		String message = "<html>You did it!</header><br/>";
		message +="Good job " + GameApplication.getPlayer().getName() + "!<br/>";
		message +="You have hired the main people for your indie game development team.<br/>";
		message +="Now you can get your team to start creating the videogame while you, as<br/>";
		message +="a producer, must handle the business part of the videogame.<br/>";
		message +="Have fun planning marketing strategies and economic assessments<br/>";
		message +="as well as motivating your team to get their assignments done by the deadline!<br/>";
		message +="<br/>";
		message +="A good programmer is someone who looks both ways<br/>";
		message +="before crossing a one-way street..<br/>";
		message +="                                        " + "-Doug Linder<br/>";
		message +="<br/>";
		message +="Game made by Subin and Angela<br/>";
		message +="</html>";
		return message;
	
	}
	

	
}