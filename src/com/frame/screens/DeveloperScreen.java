package com.frame.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.Constants;
import com.frame.GameFrame;

public class DeveloperScreen extends JPanel {
	
	private JLabel infoLabel;
	
	private JButton nextButton;
	
	public DeveloperScreen() {
		setSize(1000, 720);
		setLayout(new BorderLayout());
		setBackground(Color.white);
		
		infoLabel = new JLabel(getInstructions(), SwingConstants.CENTER);
		infoLabel.setBounds(100, 100, 600, 250);
		infoLabel.setFont(Constants.APP_FONT.deriveFont(20F));
		add(infoLabel, BorderLayout.CENTER);
		
		nextButton = new JButton("NEXT");
		nextButton.setBackground(Color.pink);
		nextButton.setForeground(Color.white);
		nextButton.setFocusPainted(false);
		nextButton.setFont(Constants.APP_FONT.deriveFont(20F));
		nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				GameFrame.setScreen(new DeveloperChoose());
			}
		});
		add(nextButton, BorderLayout.SOUTH);
	}
	
	public String getInstructions() {
		//Concatenation or concat - which means adding value to an existing value
		//in a string variable.
		String message = "<html><header>GAME DEVELOPERS</header><br/>";
		message += "If you want to bring a game to life, you'll need a game developer.<br/>";
		message += "A game developer types up lines and lines of code <br/>";
		message +="using programming language(s) such as Java, C++, C#, and etc..<br/>";
		message += "Using the programming language(s), they create the game's features and logic.<br/>";
		message += "They need to be patient, have good problem-solving skills, know programming <br/>";
		message += "languages, possess creativity, and can work well under pressure from strict deadlines.<br/>";
		
		message += "</html>";
		return message;
	}

}