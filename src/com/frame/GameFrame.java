package com.frame;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import com.Constants;
import com.frame.screens.SplashScreen;

public class GameFrame {

	private static JFrame frame;
	
	/**
	 * Initializes the game's window frame.
	 */
	public static void init() {
		frame = new JFrame();
		frame.setTitle(Constants.NAME);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setSize(1000, 720);
		frame.setVisible(true);
		setScreen(new SplashScreen());
	}
	
	/**
	 * Updates the current screen or panel of the frame to another.
	 */
	public static void setScreen(JPanel panel) {
		frame.setContentPane(panel);
		frame.repaint();
		frame.setVisible(true);
	}
	
}