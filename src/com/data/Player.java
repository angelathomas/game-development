package com.data;

/**
 * The current player class object.
 */
public class Player {
	
	/**
	 * The name of the player.
	 */
	private String name;
	
	/**
	 * Sets the name of the player.
	 * @param name The name of the player.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the name of the player.
	 * @return The name of the player.
	 */
	public String getName() {
		return name;
	}

}