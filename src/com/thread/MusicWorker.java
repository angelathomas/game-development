package com.thread;

import java.net.URL;

import com.GameApplication;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class MusicWorker implements Runnable {
	
	/**
	 * The music thread.
	 */
	private static Thread thread;
	
	/**
	 * The audio clip object.
	 */
	private static MediaPlayer player;

	/**
	 * Starts the music system thread.
	 */
	public static void start() {
		try {
			URL url = GameApplication.class.getResource("../kalimba.mp3");
			Media clip = new Media(url.toURI().toString());
			player = new MediaPlayer(clip);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		thread = new Thread(new MusicWorker());
		thread.start();
	}
	
	/**
	 * Stops the music system thread.
	 */
	public static void stop() {
		if (player != null) {
			player.stop();
		}
		thread = null;
	}

	/**
	 * Runs the thread.
	 */
	public void run() {
		if (player != null) {
			player.play();
		}
	}

}