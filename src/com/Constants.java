package com;

import java.awt.Font;


/**
 * The class that helps keep constant variables.
 * @author Angel
 */
public class Constants {
	
	/**
	 * The name of the game.
	 */
	public static final String NAME = "Game Team Creator";
	
	public static final Font APP_FONT = new Font("Lucida Sans", Font.PLAIN, 14);
	
}